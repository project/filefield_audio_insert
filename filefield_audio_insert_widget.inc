<?php
/**
 * @file
 *
 * Filefield Audio Insert cck widget settings
 */
function filefield_audio_insert_widget_process($element, $edit, &$form_state, $form) {
  //Make sure we only add js settings once.
  static $js_added;

  if (!isset($js_added)) {
    $js_added = array();

    $settings = array();

    //Get filfield audio insert cck settings for this content type.
    $field = content_fields($element['#field_name'], $element['#type_name']);

    //Add mp3player setting to js settings
    $settings['player'] = $field['widget']['filefield_audio_insert_players'];

     //Need to force width and height to the a or span tag because we are settings the as a css background.
    if ($field['widget']['filefield_audio_insert_image_use'] == 1 && !empty($field['widget']['filefield_audio_insert_default_image']['filepath'])) {
      $settings['insert_image'] = '/' . $field['widget']['filefield_audio_insert_default_image']['filepath'];
      $settings['insert_width'] = $field['widget']['filefield_audio_insert_default_image']['imageinfo'][0];
      $settings['insert_height'] = $field['widget']['filefield_audio_insert_default_image']['imageinfo'][1];
    }

    //Link image or filename to the file, so it will degraded gracefully if javascript is disabled.
    if ($field['widget']['filefield_audio_insert_link_to_file'] == 1) {
      $settings['filefield_audio_insert_link_to_file'] = TRUE;
    }

    drupal_add_js(array('filefield_audio_insert' => $settings), 'setting');
    drupal_add_js(drupal_get_path('module', 'filefield_audio_insert') .'/filefield_audio_insert.js');
  }

  //Only add insert button after file has been uploaded, after the ahah response from fielfield.
  if (isset($element['preview']) && $element['#value']['fid'] != 0) {
    $element['filefield_audio_insert_button_break'] = array(
      '#value' => '<br/><br/>',
      '#weight' => 199,
    );
  $element['filefield_audio_insert_button'] = array(
    '#type' => 'button',
    '#value' => t('Insert Audio'),
    '#weight' => 200,
    '#attributes' => array('onclick' => 'return false;' , 'file_id' => $element['#value']['fid'] , 'class' => 'filefield_audio_insert_button filefield_audio_insert_file_id-' . $element['#value']['fid']),
    );
  }

  return $element;
}


function theme_filefield_audio_insert_widget($element) {
  return theme('form_element' , $element, $element['#children']);
}
