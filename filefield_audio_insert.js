(function ($) {

/**
 * Behavior to add "Insert" buttons.
 */
Drupal.behaviors.filefield_audio_insert = function(context) {
  if (typeof(insertTextarea) == 'undefined') {
    insertTextarea = $('#edit-body').get(0) || false;
  }

  // Keep track of the last active textarea (if not using WYSIWYG).
  $('.node-form textarea:not([name$="[data][title]"])', context).focus(insertSetActive).blur(insertRemoveActive);

  $('.filefield_audio_insert_button', context).unbind('click').click(function(){
	  Drupal.filefield_audio_insert.filefield_audio_insert($(this).attr('file_id'));
	  
  });

  function insertSetActive() {
    insertTextarea = this;
    this.insertHasFocus = true;
  }

  function insertRemoveActive() {
    if (insertTextarea == this) {
      var thisTextarea = this;
      setTimeout(function() {
        thisTextarea.insertHasFocus = false;
      }, 1000);
    }
  }

 
};

// General Insert API functions.
Drupal.filefield_audio_insert = {
  /**
   * Insert content into the current (or last active) editor on the page. This
   * should work with most WYSIWYGs as well as plain textareas.
   *
   * @param content
   */
  
  filefield_audio_insert: function (fid, delta) {
    
	
	var file = new Array();
	if(Drupal.settings.filefield_audio_insert.filefield_audio_insert_link_to_file){
		$.ajax({
			url:Drupal.settings.basePath + '?q=filefield_audio_insert/file_link/' + fid, 
			timeout:10000,
			error: function(){
				alert(Drupal.t('Filefield insert audio encounter and problem.'));
			}, 
			success: function(response){
				var result = Drupal.parseJson(response);
				file['filepath'] = result.filepath;
				file['filename'] = result.filename;
				
				var content = '';
				content += '<a player="'+Drupal.settings.filefield_audio_insert.player+'" class="filefield_audio_insert_player" id="filefield_audio_insert_player-'+ fid +'" href="' + file['filepath'] + '"';
				
				if(Drupal.settings.filefield_audio_insert.insert_image){
					content += ' ><img src="'+Drupal.settings.filefield_audio_insert.insert_image+'" style="'+Drupal.settings.filefield_audio_insert.insert_width+'px; height:'+Drupal.settings.filefield_audio_insert.insert_height+'px; border:none;" /></a>';
				
				} else {
				
					content += ' >'+ file['filename']+'</a>';

				}

				
				Drupal.filefield_audio_insert.insertIntoActiveEditor(content);
			}
		});
	} else {
		$.ajax({
			url:Drupal.settings.basePath + '?q=filefield_audio_insert/file_link/' + fid, 
			timeout:10000,
			error: function(){
				alert(Drupal.t('Filefield insert audio encounter and problem.'));
			}, 
			success: function(response){
				var result = Drupal.parseJson(response);
				file['filepath'] = result.filepath;
				file['filename'] = result.filename;
				
				var content = '';
				content += '<span player="'+Drupal.settings.filefield_audio_insert.player+'" class="filefield_audio_insert_player" id="filefield_audio_insert_player-'+ fid +'" href="' + file['filepath'] + '"';
				
				if(Drupal.settings.filefield_audio_insert.insert_image){
					content += ' ><img src="'+Drupal.settings.filefield_audio_insert.insert_image+'" style="'+Drupal.settings.filefield_audio_insert.insert_width+'px; height:'+Drupal.settings.filefield_audio_insert.insert_height+'px; border:none;" /></a>';
				
				} else {
				
					content += ' >'+ file['filename']+'</span>';

				}
        
				Drupal.filefield_audio_insert.insertIntoActiveEditor(content);
			}
		});
	
	}
	
	
	
    return false;
  },
  
  get_file_path: function(fid) {
	
	
  },
  
  insertIntoActiveEditor: function(content) {

    // Always work in normal text areas that currently have focus.
    if (insertTextarea && insertTextarea.insertHasFocus) {
      Drupal.filefield_audio_insert.insertAtCursor(insertTextarea, content);
    }
    // Direct tinyMCE support.
    else if (typeof(tinyMCE) != 'undefined' && tinyMCE.activeEditor) {
      Drupal.filefield_audio_insert.activateTabPane(document.getElementById(tinyMCE.activeEditor.editorId));
      tinyMCE.activeEditor.execCommand('mceInsertContent', false, content);
    }
    // WYSIWYG support, should work in all editors if available.
    else if (Drupal.wysiwyg && Drupal.wysiwyg.activeId) {
      Drupal.filefield_audio_insert.activateTabPane(document.getElementById(Drupal.wysiwyg.activeId));
      Drupal.wysiwyg.instances[Drupal.wysiwyg.activeId].insert(content);
    }
    // FCKeditor module support.
    else if (typeof(FCKeditorAPI) != 'undefined' && typeof(fckActiveId) != 'undefined') {
      Drupal.filefield_audio_insert.activateTabPane(document.getElementById(fckActiveId));
      FCKeditorAPI.Instances[fckActiveId].InsertHtml(content);
    }
    // Direct FCKeditor support (only body field supported).
    else if (typeof(FCKeditorAPI) != 'undefined') {
      // Try inserting into the body.
      if (FCKeditorAPI.Instances[insertTextarea.id]) {
        Drupal.filefield_audio_insert.activateTabPane(insertTextarea);
        FCKeditorAPI.Instances[insertTextarea.id].InsertHtml(content);
      }
      // Try inserting into the first instance we find (may occur with very
      // old versions of FCKeditor).
      else {
        for (var n in FCKeditorAPI.Instances) {
          Drupal.filefield_audio_insert.activateTabPane(document.getElementById(n));
          FCKeditorAPI.Instances[n].InsertHtml(content);
          break;
        }
      }
    }
    // Direct CKeditor support (only body field supported).
    else if (typeof(CKEDITOR) != 'undefined' && CKEDITOR.instances[insertTextarea.id]) {
      Drupal.filefield_audio_insert.activateTabPane(insertTextarea);
      CKEDITOR.instances[insertTextarea.id].insertHtml(content);
    }
    else if (insertTextarea) {
      Drupal.filefield_audio_insert.activateTabPane(insertTextarea);
      Drupal.filefield_audio_insert.insertAtCursor(insertTextarea, content);
    }

    return false;
  },

  /**
   * Check for vertical tabs and activate the pane containing the editor.
   *
   * @param editor
   *   The DOM object of the editor that will be checked.
   */
  activateTabPane: function(editor) {
    var $pane = $(editor).parents('.vertical-tabs-pane:first');
    var $panes = $pane.parent('.vertical-tabs-panes');
    var $tabs = $panes.parents('.vertical-tabs:first').find('ul.vertical-tabs-list:first li a');
    if ($pane.size() && $pane.is(':hidden') && $panes.size() && $tabs.size()) {
      var index = $panes.children().index($pane);
      $tabs.eq(index).click();
    }
  },

  /**
   * Insert content into a textarea at the current cursor position.
   *
   * @param editor
   *   The DOM object of the textarea that will receive the text.
   * @param content
   *   The string to be inserted.
   */
  insertAtCursor: function(editor, content) {
    // Record the current scroll position.
    var scroll = editor.scrollTop;

    // IE support.
    if (document.selection) {
      editor.focus();
      sel = document.selection.createRange();
      sel.text = content;
    }

    // Mozilla/Firefox/Netscape 7+ support.
    else if (editor.selectionStart || editor.selectionStart == '0') {
      var startPos = editor.selectionStart;
      var endPos = editor.selectionEnd;
      editor.value = editor.value.substring(0, startPos) + content + editor.value.substring(endPos, editor.value.length);
    }

    // Fallback, just add to the end of the content.
    else {
      editor.value += content;
    }

    // Ensure the textarea does not unexpectedly scroll.
    editor.scrollTop = scroll;
  }
};

})(jQuery);
