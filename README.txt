
The Filefield audio insert module is an extension of the CCK Filefield module. This module allows content creators to insert audio files into nodes. This module is dependent on the mp3player module to handle embedding of flash on the node view.

Features:
  - Customizable player options through use of mp3player
  - Can optionally link the inserted code to the file. This will help with when viewing users don't have javascript enabled.
  - Allows for uploading of photo to insert, instead of inserting the file name.

6.x-1.0 Future Release:
  - Add token functionality

Dependencies
------------
 * Content
 * Filefield
 * Mp3player

Install
-------

1) Copy the filefield_audio_insert folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

3) Create a new file field in through CCK's interface. Visit Administer ->
   Content management -> Content types (admin/content/types), then click
   Manage fields on the type you want to add an file upload field. Select
   "File" as the field type and "Audio File Insert" as the widget type to create a new
   field.

4) Upload files on the node form for the type. Place cursor in the text area that you want to insert to. Click the insert button for that piece of audio.

5) Click save and see your player appear!

!!!!NOTE!!!!
Make sure you follow the mp3player installation instruction and download the player, other wise this module will not work.

Also make sure that

