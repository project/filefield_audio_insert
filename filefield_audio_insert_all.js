Drupal.behaviors.filefield_audio_insert_all = function(context){

//Need to start are delta after any default mp3players have been rendered
var delta = $('.mp3player').size() + 1;


$('.filefield_audio_insert_player', context).each(function(){
	var $this = $(this);
  var $fid = $(this).attr('id');
  var fid = $fid.split('-');
  
  $.ajax({
		url: Drupal.settings.basePath + '?q=filefield_audio_insert/embed/' + fid[1] +'/'+delta +'/'+$(this).attr('player'),
		success: function(response){
			var result = Drupal.parseJson(response);
		  $this.replaceWith(result.embed);
      $('.mp3player:not(.filefield-audio-insert-processed)').each(function() {
        var $thisPlayer = $(this);
        var playerID = $thisPlayer.attr('id');
        var playerData = $thisPlayer.attr('data');
        $thisPlayer.addClass('filefield-audio-insert-processed');
      
        var playerSettings = new Array();
      
        var playerSettingParts = playerData.split("&");
      
        for ( var i in playerSettingParts) {
          var parts = playerSettingParts[i].split("=");
          playerSettings[parts[0]] = parts[1];
        }
      
        // Create new player and add code
        var newPlayer = AudioPlayer.embed(playerID, playerSettings);
    });
		}
	
	});
	delta++;
	
});




}
